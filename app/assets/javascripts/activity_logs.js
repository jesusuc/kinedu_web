$(document).ready(function () {
    load_activity_log_data();
    $("#search_activity_logs_btn").click(function (e) {
        e.preventDefault();
        load_activity_log_data();
    });
});

const load_activity_log_data = () => {
    $.ajax({
        url: $('#search_activity_logs_url').val(),
        dataType: "html",
        data: {
            baby_id: $("#baby_select").val(),
            assistant_id: $("#assistant_select").val(),
            status: $("#status_select").val()
        },
        success: function(data) {
            $("#activity_log_data_div").html("");
            $("#activity_log_data_div").html(data);
            $("#activity_logs_tb").DataTable({"aaSorting": []});
        },
        error: function(data) {
            alert("Error de conexión contacte a soporte");
        }
    });
};