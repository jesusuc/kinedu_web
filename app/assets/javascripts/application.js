// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require rails-ujs
//= require turbolinks
//= require bootstrap
//= require datatables
//= require activestorage
//= require_tree .
//= require popper

$.extend( true, $.fn.dataTable.defaults, {
    "aaSorting": [],
    "language": {
        "lengthMenu": "Mostrando _MENU_ registros por pagina",
        "zeroRecords": "No se encontraron registros",
        "info": "Mostrando pagina _PAGE_ de _PAGES_",
        "loadingRecords": "Cargando...",
        "infoEmpty": "No se encontro información",
        "search": "",
        "searchPlaceholder": "Buscar",
        "infoFiltered": "(Filtrando desde _MAX_ total registros)",
        "paginate": {
            "previous": "<",
            "next": ">",
            "first": "...",
            "last": "..."
        }
    }
} );