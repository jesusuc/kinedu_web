class ActivityLogsController < ApplicationController
  include RemotesHelper
  def index
    @babies = get_babies
    @assistants = get_assistants
  end

  def activity_log_data
    @activity_logs = get_activity_logs(params)
    render :layout => false
  end


end
