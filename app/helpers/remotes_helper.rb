module RemotesHelper
  include HTTParty
  def get_activity_logs(params)
    data_response = []
    response = HTTParty.get((RemoteConfig.find_by_key('kinedu_ws').value + "/api/activity_logs?baby_id=#{params[:baby_id]}&assistant_id=#{params[:assistant_id]}&status=#{params[:status]}"), :headers => {"authorization" => 'K1N3DU'})
    data = JSON.parse response.body
    if data["status"] == "ok"
      data_response = data["response"]
    end
    data_response
  end

  def get_babies
    data_response = []
    response = HTTParty.get((RemoteConfig.find_by_key('kinedu_ws').value + '/api/babies'), :headers => {"authorization" => 'K1N3DU'})
    data = JSON.parse response.body
    if data["status"] == "ok"
      data_response = data["response"]
    end
    data_response
  end

  def get_assistants
    data_response = []
    response = HTTParty.get((RemoteConfig.find_by_key('kinedu_ws').value + '/api/assistants'), :headers => {"authorization" => 'K1N3DU'})
    data = JSON.parse response.body
    if data["status"] == "ok"
      data_response = data["response"]
    end
    data_response
  end
end