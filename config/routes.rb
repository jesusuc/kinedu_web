Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'home#index'

  resources :activity_logs do
    collection do
      get 'activity_logs_data', to: 'activity_logs#activity_log_data'
    end
  end
end
